import React, {Component} from "react"
import ToDoInput from "../ToDoInput"
import ToDoSubmitBtn from "../ToDoSubmitBtn"
import ToDoList from "../ToDoList"

class App extends Component{

    constructor(props){
        super(props)

        this.state={
            toDoElems: []
        }    

        this.clickSubmit = this.clickSubmit.bind(this);
        this.clickDelete = this.clickDelete.bind(this);

    }

    clickSubmit = () => {

        const inputValue = document.getElementById("toDoInput")

        this.setState(state => {
            const toDoElems = [...state.toDoElems, inputValue.value];
                return {
                    toDoElems,
                    value: '',
                }
        })

    }

    clickDelete = elemId => {

        this.setState(state => {
          const toDoElems = state.toDoElems.filter(((item,key) => {
            return key !== elemId}));
          return {
            toDoElems,
          };
        });
      };

    render(){
        return(
            <div>
                <ToDoInput></ToDoInput>
                <ToDoSubmitBtn onClick={this.clickSubmit}></ToDoSubmitBtn>
                <ToDoList toDoElems={this.state.toDoElems} clickDelete={this.clickDelete}></ToDoList>
            </div>
        )
    }


}

export default App