import React, {Component} from "react"
import {render} from "react-dom"
import './style.css'

class ToDoListElem extends Component{

    constructor(props){
        super(props)

        this.state = {
            done: false
        }

        this.clickMakeDone = this.clickMakeDone.bind(this);

    }

    clickMakeDone = () => {
        debugger
        !this.state.done && this.setState({done: true})
    }

    render(){

        const {elem, itemId, onClick} = this.props

        return(
            <li onClick={this.clickMakeDone}>

                {
                    this.state.done === true 
                    ? 
                    <span className="done">{elem}</span>
                    :
                    <span>{elem}</span>
                }

                <button onClick={onClick.bind(this, itemId)}>Delete</button>
            </li>
        )

    }


}

export default ToDoListElem