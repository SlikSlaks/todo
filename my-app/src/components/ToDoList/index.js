import React, {Component} from "react"
import {render} from "react-dom"
import ToDoListElem from "../ToDoListElem"

class ToDoList extends Component{

    constructor(props){
        super(props)
    }

    render(){

        const {toDoElems, clickDelete} = this.props

        const list = toDoElems.map( (elem, key ) => 
            <ToDoListElem elem={elem} itemId={key} onClick={clickDelete}/>
         )

        return(
            <ul>
                {list}
            </ul>
        )

    }


}

export default ToDoList