import React, {Component} from "react"
import {render} from "react-dom"

class ToDoSubmitBtn extends Component{

    constructor(props){
        super(props)
    }

    render(){

        const {onClick} = this.props

        return(
            <button onClick={onClick}>Save</button>
        )

    }

}

export default ToDoSubmitBtn